import java.util.Scanner;
public class Examen1 {

	public static void main(String[] args) {
		int seleccion = 0, numeros=0, lado=0, ladoTotal = 0;
		String seleccionStr, palabra, numeroStr, ladoStr;
		Boolean comprobacion;
		float numero = 0f, suma = 0f;
		
		Scanner lectura = new Scanner(System.in);
		do {
		System.out.println("MENU");
		System.out.println("====");
		System.out.println("1. Palabra invertida");
		System.out.println("2. Tri�ngulo n�meros");
		System.out.println("3. Mayor/menor");
		System.out.println("4. Salir");
		System.out.println(" ");
		
		do {
		comprobacion = true;
			try {
				System.out.print("Introduce una opci�n: ");
				seleccionStr = lectura.nextLine();
				seleccion = Integer.parseInt(seleccionStr);
				if(seleccion < 0) {
					System.out.println("No has introducido ning�n n�mero v�lido...");
					comprobacion = false;
				}
				if(seleccion > 4) {
					System.out.println("Opci�n incorrecta, vuelve a intentarlo por favor...");
					comprobacion = false;
				}
			}catch(Exception e) {
				System.out.println("Opcion incorrecta, vuelve a intentarlo por favor...");
				comprobacion = false;
			}
		}while(comprobacion != true);
		
		switch(seleccion){
		case 1:
			System.out.print("Introduce una palabra: ");
			palabra = lectura.nextLine();
			for (int i=(palabra.length()-1);i>=0;i-=1) {
				System.out.print(palabra.toUpperCase().charAt(i));
			}
				System.out.println(" ");
			break;
		case 2:
			do {
				comprobacion = true;
				try {
					System.out.print("Introduce el tama�o del lado del tri�ngulo: ");
					ladoStr = lectura.nextLine();
					lado = Integer.parseInt(ladoStr);
					ladoTotal = lado;
					
				}catch(Exception e) {
					System.out.println("El valor introducido no es un n�mero, vuelve a intentarlo... ");
					comprobacion = false;
				}
			}while(comprobacion != true);
			
			for(int i=1;i<=ladoTotal;i++) {
				for (int j=1;j<=lado;j++) {
					System.out.print(j+ " ");
				}
				System.out.println(" ");
				lado -= 1;
			}
			break;
			
			/* =========== As� lo ha hecho �l, yo lo hice peor xd ==============
			for(int i=0; i<lado;i++) {
				for (int j=1; j<=(lado-i); j++) {
					System.out.println(j+" ");
				}
				System.out.println("");
			}
			================================================================= */
			
		case 3:
			do {
				do {
					comprobacion = true;
					try {
						System.out.print("Introduce un n�mero (Negativo para salir del programa): ");
						numeroStr = lectura.nextLine();
						numero = Float.parseFloat(numeroStr);
						
					}catch(Exception e) {
						System.out.println("El valor introducido no es un n�mero, vuelve a intentarlo... ");
						comprobacion = false;
					}
				}while(comprobacion != true);
				if(numero>0) {
					suma += numero;
					numeros += 1;
				}
			}while(numero>0);
			System.out.println("La suma de los n�meros es: "+suma);
			System.out.println("La media de los n�meros es: "+(suma/numeros));
			break;
			
		case 4:
			System.out.println("Saliendo...");
			break;
		}
		System.out.println(" ");
		}while(seleccion != 4);
	}

}
